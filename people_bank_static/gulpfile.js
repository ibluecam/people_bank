var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

gulp.task('serve',['minify-css'], function(){
	browserSync.init({
    	server: "./"
  	})
  	
  	gulp.watch('./*.html').on('change', reload);
  	gulp.watch('./pre-css/*.css', ['minify-css']); 
  	gulp.watch('./pre-css/*.css').on('change', reload);
});

gulp.task('minify-css', function(){
	 gulp.src('./pre-css/*.css')
	.pipe(minifyCSS({
			//keepSpecialComments: 1 
			keepBreaks: true
		}))
	.pipe(gulp.dest('css'));
});

gulp.task('default', ['serve']);

